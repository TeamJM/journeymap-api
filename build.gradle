/*
 * JourneyMap API (http://journeymap.info)
 * http://bitbucket.org/TeamJM/journeymap-api
 *
 * Copyright (c) 2011-2016 Techbrew.  All Rights Reserved.
 * The following limited rights are granted to you:
 *
 * You MAY:
 *  + Write your own code that uses the API source code in journeymap.* packages as a dependency.
 *  + Write and distribute your own code that uses, modifies, or extends the example source code in example.* packages
 *  + Fork and modify any source code for the purpose of submitting Pull Requests to the TeamJM/journeymap-api repository.
 *    Submitting new or modified code to the repository means that you are granting Techbrew all rights to the submitted code.
 *
 * You MAY NOT:
 *  - Distribute source code or classes (whether modified or not) from journeymap.* packages.
 *  - Submit any code to the TeamJM/journeymap-api repository with a different license than this one.
 *  - Use code or artifacts from the repository in any way not explicitly granted by this license.
 *
 */

/* Repos */
buildscript {
    repositories {
        jcenter()
        maven {
            name = "forge"
            url = "http://files.minecraftforge.net/maven"
        }
        maven {
            url "https://plugins.gradle.org/m2/"
        }
    }
    dependencies {
        classpath group: 'net.minecraftforge.gradle', name: 'ForgeGradle', version: '3.+', changing: true
    }
}

/* Gradle Plugins */
apply plugin: 'net.minecraftforge.gradle'
apply plugin: 'eclipse'
apply plugin: 'maven'
apply plugin: 'signing'

/* Artifact Version */
version = project.minecraft_version + '-' + project.api_version

/* Java 8 */
sourceCompatibility = 1.8
targetCompatibility = 1.8

/* ForgeGradle spec for Minecraft */
minecraft {
//    version = project.minecraft_version + "-" + project.forge_version
//    runDir = "run"
    mappings channel: 'snapshot', version: project.mappings
//    replace '@API_VERSION@', project.api_version
//    replace '@MC_VERSION@', project.minecraft_version

    runs {
        client = {
            // recommended logging data for a userdev environment
            properties 'forge.logging.markers': 'SCAN,REGISTRIES,REGISTRYDUMP'
            // recommended logging level for the console
            properties 'forge.logging.console.level': 'info'
            workingDirectory project.file('run/client').canonicalPath
            source sourceSets.main
        }
        server = {
            // recommended logging data for a userdev environment
            properties 'forge.logging.markers': 'SCAN,REGISTRIES,REGISTRYDUMP'
            // recommended logging level for the console
            properties 'forge.logging.console.level': 'debug'
            workingDirectory project.file('run').canonicalPath
            source sourceSets.main
        }
    }
}
dependencies {
    minecraft "net.minecraftforge:forge:${project.minecraft_version}-${project.forge_version}"
}
/* Replace tokens in resources files */
processResources
        {
            // this will ensure that this task is redone when the versions change.
            inputs.property "version", project.version
            inputs.property "mcversion", project.minecraft_version

            // replace stuff in mcmod.info on copy
            from(sourceSets.main.resources.srcDirs) {
                include 'mcmod.info'
                expand 'version':project.version, 'mcversion':project.minecraft_version
            }

            // copy everything else
            from(sourceSets.main.resources.srcDirs) {
                exclude 'mcmod.info'
            }
        }

/* Reobfuscated jar for just the Example Mod */
jar {
    dependsOn classes
    from sourceSets.main.output
    classifier = 'examplemod'
    include 'example/**'
    include 'mcmod.info'
    include 'assets/**'
    manifest {
        attributes = [
                "Manifest-Version": "1.0",
                "Implementation-Title": project.title_example,
                "Implementation-Version": project.version,
                "Implementation-URL"  : project.git_website
        ]
    }
}

/* Jar for just the API - Stays deobfuscated so it can be added as a mod project dependency */
task deobfJar(type: Jar, dependsOn: classes) {
    mustRunAfter = ['jar']
    from sourceSets.main.output
    include 'journeymap/**'
    manifest {
        attributes = [
                "Manifest-Version": "1.0",
                "Implementation-Title": project.title,
                "Implementation-Version": project.version,
                "Implementation-URL"  : project.git_website
        ]
    }
}

/* Javadoc properties */
javadoc {
    title = project.title_javadoc
}

/* Jar for Javadocs */
task javadocJar(type: Jar, dependsOn: javadoc) {
    classifier = 'javadoc'
    from javadoc.destinationDir
}

/* Jar for Sources */
task sourcesJar(type: Jar) {
    classifier = 'sources'
    from sourceSets.main.allSource
}

/* Additional artifacts produced by build task */
artifacts {
    archives jar
    archives deobfJar
    archives javadocJar
    archives sourcesJar
}

/* Jar signing (http://central.sonatype.org/pages/gradle.html) */
signing {
    sign configurations.archives
}

/* Central Maven Deployment (only used by TeamJM) */
afterEvaluate { project ->
    if (project.hasProperty("sonatypeUsername")) {
        uploadArchives {
            repositories {
                mavenDeployer {
                    beforeDeployment { MavenDeployment deployment -> signing.signPom(deployment) }

                    repository(url: "https://oss.sonatype.org/service/local/staging/deploy/maven2/") {
                        authentication(userName: sonatypeUsername, password: sonatypePassword)
                    }

                    snapshotRepository(url: "https://oss.sonatype.org/content/repositories/snapshots/") {
                        authentication(userName: sonatypeUsername, password: sonatypePassword)
                    }

                    pom.project {
                        name = project.title
                        description = project.description
                        url = project.git_website
                        packaging = 'jar'

                        scm {
                            connection project.gir_scm
                            developerConnection project.gir_scm
                            url project.git_url
                        }

                        licenses {
                            license {
                                name 'Copyright'
                                url project.license_url
                            }
                        }

                        developers {
                            developer {
                                id 'techbrew'
                                name 'Techbrew'
                                email 'techbrew@journeymap.info'
                            }
                        }
                    }
                }
            }
        }
    }
}